#include "catmull_clark.h"
#include <unordered_map>
#include <utility>
#include <functional>
#include <map>
#include <vector>
#include <string>

void catmull_clark(
  const Eigen::MatrixXd & V,
  const Eigen::MatrixXi & F,
  const int num_iters,
  Eigen::MatrixXd & SV,
  Eigen::MatrixXi & SF)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:

  //I probably could have optimized my maps to use less of them and search through them less, but while 
  //designing this algorithm I was having trouble keeping track of indexes and wanted to have very clear references to what was what

  //I also stored some of the values, like F and R, in maps to make it less of a mess in each block of code. That way, when moving the points,
  //I only needed to worry about references instead of doing all the calculations in the block. Just a heads up to whoever looks at this
  //and is taken aback by the large quantity of maps 
    SV = V;
    SF = F;

    

    if (num_iters == 0) {
            return;
        }

    //convert a point to a string, used to hash points in unordered map
    auto point_to_string = [](Eigen::Vector3d point){
        std::string key = 
            std::to_string(point(0)) + "," + 
            std::to_string(point(1)) + "," + 
            std::to_string(point(2));
            return key;
    };

    //face points
    std::map<int, Eigen::RowVector3d> Face_Points;
    for (int i = 0; i < F.rows(); i ++){
        double x = (V(F(i,0),0) + V(F(i,1),0) + V(F(i,2),0) + V(F(i,3),0))/4;
        double y = (V(F(i,0),1) + V(F(i,1),1) + V(F(i,2),1) + V(F(i,3),1))/4;
        double z = (V(F(i,0),2) + V(F(i,1),2) + V(F(i,2),2) + V(F(i,3),2))/4;
        Eigen::RowVector3d point(x, y, z);
        Face_Points[i] = point;
    }

    // faces that a point belongs to
    std::map<int, std::vector<int>> point_faces;
    for (int i=0; i<F.rows(); i++) {
        for (int j=0; j<F.cols(); j++) {
            point_faces[F(i, j)].push_back(i);
        }
    }

    // the neighbours of a point
    std::map<int, std::vector<int>> Neighbours;
    for (int i=0; i<F.rows(); i++) {
        for (int j=0; j<F.cols(); j++) {
            Neighbours[F(i, j)].push_back(F(i, (j+1)%F.cols()));
        }
    }


    //faces that connect edges
    std::map<std::pair<int, int>, std::vector<int>> edge_faces;
    for (int i=0; i<F.rows(); i++) {
        for (int j=0; j<F.cols(); j++) {
            edge_faces[std::make_pair(F(i, j), F(i, (j+1)%F.cols()))].push_back(i);
            edge_faces[std::make_pair(F(i, (j+1)%F.cols()), F(i, j))].push_back(i);
        }
    }

    //edge points
    std::map<std::pair<int, int>, Eigen::RowVector3d> Edge_Points;
    for (const auto &p : edge_faces){
        Eigen::RowVector3d total(0,0,0);
        for (int i = 0; i < p.second.size(); i++){
            total += Face_Points[p.second[i]];
        }
        total += V.row(p.first.first) + V.row(p.first.second);
        total = total / 4;
        Edge_Points[p.first] = total;
    }

    //Average of facepoints adjacent to P, F
    std::map<int, Eigen::RowVector3d> F_Average;
    for (const auto &p : point_faces){
        Eigen::RowVector3d total(0,0,0);
        for (int i = 0; i < p.second.size(); i++){
            total += Face_Points[p.second[i]];
        }
        F_Average[p.first] = total/p.second.size();
    }

    //Average of midpoints touching P, R
    std::map<int, Eigen::RowVector3d> R;
    for (const auto &p : Neighbours){
        Eigen::RowVector3d total(0,0,0);
        for (int i = 0; i < p.second.size(); i++){
            total += (V.row(p.first) + V.row(p.second[i]))/2;
        }
        R[p.first] = total/p.second.size();
    }

    std::unordered_map<std::string, int> index; //indexes for a vertices

    SV.resize(V.rows() + edge_faces.size()/2 + F.rows(),3);
    SF.resize(F.rows() * 4, 4);
    //Move each original point according to formula (F + 2R + (n-3) * P)/n
    int idx = 0;
    for (int i = 0; i < V.rows(); i ++){
        Eigen::RowVector3d new_pos(0,0,0);
        new_pos = F_Average[i] + 2 * R[i] + (point_faces[i].size() - 3) * V.row(i);
        new_pos = new_pos / point_faces[i].size();
        SV.row(i) = new_pos;
        std::string key = point_to_string(new_pos);
        index[key] = i;
        idx++;
    }

    //add all the face points to SV and the index map

    for (int i = 0; i < F.rows(); i++){
        Eigen::RowVector3d face = Face_Points[i];
        SV.row(idx) = face;
        std::string key = point_to_string(face);
        index[key] = idx;
        idx++;
    }

    //add all the edge points to SV and the index map
    for (const auto &p : Edge_Points){
        std::string key = point_to_string(p.second);
        if (index.count(key) == 0){
            SV.row(idx) = p.second;
            index[key] = idx;
            idx++;
        }
    }

    //now to create the faces
    idx = 0;
    for(int i = 0; i < F.rows(); i ++){
        for (int j = 0; j < 4; j++){
            Eigen::RowVector4i new_face(0,0,0,0);
            new_face(0) = index[point_to_string(Face_Points[i])];
            std::pair<int, int> first_edge(F(i, j % 4), F(i, (j+1)%4));
            new_face(1) = index[point_to_string(Edge_Points[first_edge])];
            new_face(2) = F(i, (j+1)%4);
            std::pair<int, int> second_edge(F(i, (j + 1) % 4), F(i, (j+2)%4));
            new_face(3) = index[point_to_string(Edge_Points[second_edge])];
            SF.row(idx) = new_face;
            idx++;
        }
    }

    //Yay!, now recursion
    catmull_clark(Eigen::MatrixXd(SV), Eigen::MatrixXi(SF), num_iters-1, SV, SF);






  ////////////////////////////////////////////////////////////////////////////
}
